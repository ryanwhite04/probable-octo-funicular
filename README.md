# probable-octo-funicular

## Contributing

- Use [paper-elements](https://www.webcomponents.org/collection/polymerelements/paper-elements) from [UNPKG](https://unpkg.com/#/) after checking their [status](https://github.com/Polymer/polymer-modulizer/blob/master/docs/polymer-3-element-status.md)
  - URL should be of the form ```https://unpkg.com/@polymer/paper-[Element Name]/paper-[Element Name].js?module```
- Colors from [Material Design](https://material.io/design/color/the-color-system.html#tools-for-picking-colors)

## TODOs

- [ ] Content
  - [x] Videos
  - [x] Music
  - [ ] Articles
  - [ ] Endless Scroll
  - [ ] Load only on selecting distraction
- [x] Use local storage user profile
- [ ] Buttons!
  - [x] Save me
  - [x] Help me
  - [x] Distract me
  - [x] Profile
- [ ] Disclaimer
- [ ] Translations
  - [ ] English
  - [ ] Mandarin
  - [ ] Spanish
- [ ] Location
  - [ ] Correct Emergency Services
  - [ ] Geo Location API
  - [ ] [Emergency Numbers](http://emergencynumberapi.com/)
- [ ] Profile Creation Page
  - [ ] Personal Details
  - [ ] Emergency Contacts
  - [ ] Location
- [ ] Favicon
- [ ] Images
  - [ ] Distract Me
  - [ ] Help Me
  - [ ] Save Me
- [ ] Email through google functions
