import { html, render } from 'https://unpkg.com/lit-html@0.11.1/lit-html.js?module'

// Set up the profile if it already exists
let profile = JSON.parse(localStorage.getItem('profile')) || {};

// These links when appended with content ID can be used to embed content in an iframe
let sources = {
  vimeo: 'https://player.vimeo.com/video',
  youtube: 'https://www.youtube.com/embed',
  soundcloud: 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks',
}

// Attach onClick event listeners
Object.entries({
  'save-button': e => document.getElementById('save-dialog').toggle(),
  'distract-button': e => document.getElementById('distract-dialog').toggle(),
  'help-button': e => document.getElementById('help-dialog').toggle(),
  'profile-button': e => document.getElementById('profile-dialog').toggle(),
  'save-profile': e => localStorage.setItem('profile', JSON.stringify(profile)),
  'video-button': e => document.getElementById('video-dialog').toggle(),
  'music-button': e => document.getElementById('music-dialog').toggle(),
  // 'article-button': e => document.getElementById('article-dialog').toggle(),
}).map(([id, listener]) => document.getElementById(id).addEventListener('click', listener))

// Load profile values, and update profile on input changes
let inputs = [...document.getElementById('profile-dialog').getElementsByTagName('paper-input')];

inputs.map(input => {
  input.value = profile[input.getAttribute('name')] || ''
  input.addEventListener('change', e => {
    console.log('change', e, input)
    profile[input.getAttribute('name')] = input.value;
  })
})

// Return an iframe for embedded content
function contentTemplate({ categories, id, title, source, link }) {
  return html`<h3>${title}</h3><iframe
    width="100%"
    height="300"
    scrolling="no"
    frameborder="no"
    src="${sources[source]}/${id}">
  </iframe>`;
}

// Populate content dialogs with data from index.json
fetch('index.json').then(response => response.json()).then(content => {
  let videos = content.filter(({ categories }) => categories.includes('Watch'))
  let music = content.filter(({ categories }) => categories.includes('Listen'))
  render(html`${videos.map(contentTemplate)}`, document.getElementById('videos'))
  render(html`${music.map(contentTemplate)}`, document.getElementById('music'))
}, console.error)